let tongTien;
function renderSanPham(data){
    let contentHTML = "";
    data.forEach(function(item){
        let content = `
        <div class="col-12 col-lg-3 col-md-6">
        <div class="card py-3" style="width: 17.5rem;">
        <img src="${item.img}" class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title">${item.name}</h5>
            <p>${item.price}$</p>
            <p>${item.screen}</p>
            <p>${item.backCamera}</p>
            <p>Camera Trước: ${item.frontCamera}</p>
            <p>${item.desc}</p>
            <p>${item.type}</p>
            <button onclick="themGioHang(${item.ma})"><i class="fa fa-shopping-cart""></i></button>
            </div>
        </div>
        </div>`;
        contentHTML+=content;
    })
    document.getElementById("dssp").innerHTML = contentHTML;
}
function openSite(){
    document.getElementById("cart-site").style.display = "block";
    document.getElementById("overlay").style.display = "block"
}
function closeSite(){
    document.getElementById("cart-site").style.display = "none";
    document.getElementById("overlay").style.display = "none";
}
function renderCart(cart){
    let contentHTML = "";
    tongTien = 0;
    if(cart.length == 0){
        document.getElementById("cart-table").innerHTML = `Giỏ hàng trống. Vui lòng chọn 1 sản phẩm!`;
        document.getElementById("tongTien").innerText = `${tongTien}$`;
    }
    else{
    cart.forEach(function(item){
        let content = `
        <tr>
        <td><img src="${item.product.img}" alt="" /></td>
        <td>${item.product.name}</td>
        <td><button class="sub-btn" onclick="giamSp(${item.product.ma})"><i class="fa fa-minus"></i></button> ${item.quantity} <button class="add-btn" onclick="tangSp(${item.product.ma})"><i class="fa fa-plus"></i></button></td>
        <td>${item.product.price*item.quantity}$</td>
        <td><button class="del-btn" onclick="xoaSp(${item.product.ma})"><i class="fa fa-trash"></i></button></td>
        </tr>`
        contentHTML+=content;
        tongTien += item.product.price*item.quantity;
    })
    document.getElementById("cart-table").innerHTML = contentHTML;
    document.getElementById("tongTien").innerText = `${tongTien}$`;
    }
}
function tongGioHang(cart){
    let tong = 0;
    cart.forEach(function(item){
        tong+=item.quantity;
    })
    document.getElementById("cart-number").innerHTML = tong;
}
function kiemTraTrung(id,cart){
    for(let index = 0; index < cart.length;index++){
        if(id == cart[index].product.ma){
            return index;
        }
    }
    return -1;
}
function xoaSp(id){
    let viTri = kiemTraTrung(id,cart);
    cart.splice(viTri,1);
    luuLocalStorage();
    renderCart(cart);
    tongGioHang(cart);
}
function tangSp(id){
    let viTri = kiemTraTrung(id,cart);
    cart[viTri].quantity++;
    luuLocalStorage();
    renderCart(cart);
    tongGioHang(cart);
}
function giamSp(id){
    let viTri = kiemTraTrung(id,cart);
    cart[viTri].quantity--;
    if(cart[viTri].quantity <= 0){
        cart.splice(viTri,1)
    }
    luuLocalStorage();
    renderCart(cart);
    tongGioHang(cart);
}
function clearCart(){
    cart = [];
    luuLocalStorage();
    renderCart(cart);
    tongGioHang(cart);
}
function renderPay(cart){
    contentHTML = "";
    cart.forEach(function(item){
        let content = `
        <tr>
        <td>${item.quantity}</td>
        <td>${item.product.name}</td>
        <td>${item.quantity*item.product.price}$</td>
        </tr>
        `;
    contentHTML += content;
    })
    document.getElementById("pay-table").innerHTML = contentHTML;
    document.getElementById("tong").innerHTML = `${tongTien}$`
}