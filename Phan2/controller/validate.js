import { showMessageErr } from "./controller.js";
export function kiemTraRong(input, idErr, message) {
  if (input.length == 0) {
    showMessageErr(idErr, message);
    return false;
  } else {
    showMessageErr(idErr, "");
    return true;
  }
}
export function kiemTraSoLuong(input, idErr, messErr) {
  var reg = /^[0-9]+$/;
  let isTK = reg.test(input);
  if (isTK) {
    showMessageErr(idErr, "");
    return true;
  } else {
    showMessageErr(idErr, messErr);
    return false;
  }
}
