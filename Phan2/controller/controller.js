export function layThongTinTuForm() {
  let tenSP = document.getElementById("TenSP").value;
  let gia = document.getElementById("GiaSP").value;
  let hinhAnh = document.getElementById("HinhSP").value;
  let moTa = document.getElementById("MoTa").value;
  return { tenSP, gia, hinhAnh, moTa };
}
export function renderProductList(data) {
  let contentHTML = "";
  data.forEach((item) => {
    let { id, tenSP, gia, hinhAnh, moTa } = item;
    contentHTML += `<tr>
  <td>${id}</td>
  <td>${tenSP}</td>
  <td>${gia}</td>
  <td>${hinhAnh}</td>
  <td>${moTa}</td>
  <td><button class="btn btn-danger" onclick="xoaSP(${id})">Xoa</button> 
  <button class="btn btn-primary"  data-toggle="modal"
  data-target="#myModal" onclick="suaSP(${id})">Sua</button></td>
  </tr>`;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
}
export function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}
export function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}
export function showMessageErr(idErr, message) {
  document.getElementById(idErr).style.display = "block";
  document.getElementById(idErr).style.color = "red";
  document.getElementById(idErr).innerHTML = message;
}
