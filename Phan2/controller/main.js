import {
  getAllProduct,
  deleteProduct,
  addProduct,
  editProduct,
  updateProduct,
} from "../service/service.js";
import {
  layThongTinTuForm,
  renderProductList,
  turnOffLoading,
  turnOnLoading,
} from "./controller.js";
import { kiemTraRong, kiemTraSoLuong } from "./validate.js";
var idEdited = null;
// Fetch
let fetchAllProductService = () => {
  turnOnLoading();
  getAllProduct()
    .then((res) => {
      renderProductList(res.data);
      turnOffLoading();
      console.log("res: ", res);
    })
    .catch((err) => {
      turnOffLoading();
      console.log("err: ", err);
    });
};
fetchAllProductService();
function xoaSP(id) {
  turnOnLoading();
  deleteProduct(id)
    .then((res) => {
      turnOffLoading();
      fetchAllProductService();
      console.log("res: ", res);
    })
    .catch((err) => {
      turnOffLoading();
      fetchAllProductService();
      console.log("err: ", err);
    });
}
window.xoaSP = xoaSP;
// Add
function themSP() {
  var data = layThongTinTuForm();
  var newSP = {
    tenSP: data.tenSP,
    gia: data.gia,
    hinhAnh: data.hinhAnh,
    moTa: data.moTa,
  };
  var isValid = true;
  // validate ten SP
  isValid = kiemTraRong(
    data.tenSP,
    "tbTenSP",
    "Ten san pham khong duoc de trong"
  );
  // validate gia SP
  isValid =
    isValid &
      kiemTraRong(data.gia, "tbGiaSP", "Gia san pham khong duoc de trong") &&
    kiemTraSoLuong(data.gia, "tbGiaSP", "Gia san pham phai la so");
  // validate hinh anh
  isValid =
    isValid &
    kiemTraRong(data.hinhAnh, "tbHinh", "Hinh san pham khong duoc de trong");
  // validate mo ta
  isValid =
    isValid &
    kiemTraRong(data.moTa, "tbMoTa", "Mo ta san pham khong duoc de trong");
  turnOnLoading();
  if (isValid) {
    addProduct(newSP)
      .then((res) => {
        fetchAllProductService();
        turnOffLoading();
        console.log("res: ", res);
      })
      .catch((err) => {
        fetchAllProductService();
        turnOffLoading();
        console.log("err: ", err);
      });
  } else {
    turnOffLoading();
  }
}

window.themSP = themSP;
// Edit
function suaSP(id) {
  editProduct(id)
    .then((res) => {
      console.log("res: ", res);
      document.getElementById("TenSP").value = res.data.tenSP;
      document.getElementById("GiaSP").value = res.data.gia;
      document.getElementById("HinhSP").value = res.data.hinhAnh;
      document.getElementById("MoTa").value = res.data.moTa;
      idEdited = res.data.id;
    })
    .catch((err) => {
      console.log("err: ", err);
    });
}
window.suaSP = suaSP;
// update
function capNhapSP() {
  let data = layThongTinTuForm();
  var isValid = true;
  // validate ten SP
  isValid = kiemTraRong(
    data.tenSP,
    "tbTenSP",
    "Ten san pham khong duoc de trong"
  );
  // validate gia SP
  isValid =
    isValid &
      kiemTraRong(data.gia, "tbGiaSP", "Gia san pham khong duoc de trong") &&
    kiemTraSoLuong(data.gia, "tbGiaSP", "Gia san pham phai la so");
  // validate hinh anh
  isValid =
    isValid &
    kiemTraRong(data.hinhAnh, "tbHinh", "Hinh san pham khong duoc de trong");
  // validate mo ta
  isValid =
    isValid &
    kiemTraRong(data.moTa, "tbMoTa", "Mo ta san pham khong duoc de trong");
  turnOnLoading();
  if (isValid) {
    updateProduct(data, idEdited)
      .then(function (res) {
        console.log(res);
        fetchAllProductService();
        turnOffLoading();
      })
      .catch(function (err) {
        console.log(err);
        turnOffLoading();
      });
  } else {
    turnOffLoading();
  }
}
window.capNhapSP = capNhapSP;
