let BASE_URL = "https://635f4b1a3e8f65f283b0122e.mockapi.io";
export let getAllProduct = () => {
  return axios({
    url: `${BASE_URL}/QLSP`,
    method: "GET",
  });
};
export let deleteProduct = (id) => {
  return axios({
    url: `${BASE_URL}/QLSP/${id}`,
    method: "DELETE",
  });
};
export let addProduct = (newSP) => {
  return axios({
    url: `${BASE_URL}/QLSP`,
    method: "POST",
    data: newSP,
  });
};
export let editProduct = (id) => {
  return axios({
    url: `${BASE_URL}/QLSP/${id}`,
    method: "GET",
  });
};
export let updateProduct = (data, id) => {
  return axios({
    url: `${BASE_URL}/QLSP/${id}`,
    method: "PUT",
    data: data,
  });
};
