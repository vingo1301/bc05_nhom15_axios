const BASE_URL = "https://635f4c4b3e8f65f283b02e50.mockapi.io/";
let productList = [];
let cart = [];
function luuLocalStorage(){
    let jsonCart = JSON.stringify(cart);
    localStorage.setItem("CART",jsonCart)
}
let dataJson = localStorage.getItem("CART");
if(dataJson !== null){
    var svArr = JSON.parse(dataJson);
    for(var index = 0; index < svArr.length; index++){
        var item = svArr[index];
        cart.push(item);
        renderCart(cart);
    }
}
function fetchAllTodo(){
    axios({
        url: `${BASE_URL}/products`,
        method: "GET"
    })
    .then(function(res){
        console.log("res: ",res.data);
        res.data.forEach(function(item){
            productList.push(item);
        })
        renderSanPham(productList);
    }).catch(function(err){
        console.log("err: ", err);
        }
    );
}
fetchAllTodo();
renderCart(cart);
tongGioHang(cart);
renderPay(cart);
function locSanPham(){
    let listIphone = [];
    let listSamsung = [];
    productList.forEach(function(item){
        if(item.type === "Samsung"){
            listSamsung.push(item)
        }else{
            listIphone.push(item)
        }
    })
    let value = document.getElementById("filterSanPham").value ;
    if(value == 1){
        renderSanPham(listIphone);
    } else if(value == 2){
        renderSanPham(listSamsung);
    }else{
        renderSanPham(productList);
    }
}
function themGioHang(id){
    let cartItem = {product:{},
    quantity: 1}
    cartItem.product = productList[id-1];
    let viTri = kiemTraTrung(id,cart);
    if(viTri == -1){
        cart.push(cartItem);
    }
    else{
        cart[viTri].quantity++;
    }
    luuLocalStorage();
    renderCart(cart);
    tongGioHang(cart);
}
function openPay(){
    renderPay(cart);
    document.getElementById("pay-site").style.display = "block";
}
function closePay(){
    document.getElementById("pay-site").style.display = "none";
}
function paySuccess(){
    document.getElementById("pay-site").style.display = "none";
    closeSite();
    clearCart();
}